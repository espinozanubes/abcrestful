<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Conductor extends Model {

	protected $table="conductors";
	protected $fillable = array('nomConductor','telefono','email','password','statusId','conductor_id');
	protected $guarded = ['id'];
	protected $hidden = ['created_at','updated_at']; 

	public function vehiculo()
	{
		return $this->hasOne('App\Vehiculo');
	}
}