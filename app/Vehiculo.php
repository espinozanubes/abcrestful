<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model {

	protected $table='vehiculos';
	protected $fillable = array('marca','modelo','color','serie','idConductor','StatusId','vehiculo_id');
	protected $guarded = ['id'];
	protected $hidden = ['created_at','updated_at']; 
	public function conductor()
	{
		return $this->belongsTo('App\Conductor');
	}

}
