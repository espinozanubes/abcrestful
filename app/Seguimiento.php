<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Seguimiento extends Model {

	protected $table='seguimiento';
	protected $fillable = array('ubicacion','idConductor','idVehiculo','StatusId','vehiculo_id');
	protected $guarded = ['id'];
	protected $hidden = ['created_at','updated_at']; 
	public function vehiculo()
	{
		return $this->hasMany('App\Vehiculo');
	}

}
