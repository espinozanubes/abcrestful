<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConductorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conductors', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nomConductor');
			$table->integer('telefono');
			$table->string('email');
			$table->string('password');
			$table->integer('statusId');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conductors');
	}

}
