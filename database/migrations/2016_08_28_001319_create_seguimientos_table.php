<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeguimientosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('seguimiento', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('ubicacion');
			$table->integer('idVehiculo');

			$table->integer('vehiculo_id')->unsigned();
			$table->foreign('vehiculo_id')->references('id')->on('vehiculos');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('seguimiento');
	}

}
