<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiculosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vehiculos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('marca');
			$table->string('modelo');
			$table->string('color');
			$table->integer('serie');
			$table->integer('idConductor');
			$table->integer('StatusId');

			$table->integer('conductor_id')->unsigned();
			$table->foreign('conductor_id')->references('id')->on('conductors');

			$table->integer('vehiculo_id')->unsigned();
			$table->foreign('vehiculo_id')->references('id')->on('vehiculos');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vehiculos');
	}

}
